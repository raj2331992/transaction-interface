package requests;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class TransactionCreationRequest {

    @NotNull(message="amount type field is missing")
    @Min(value=0, message = "Amount should be positive")
    private Double amount;

    @NotNull(message="transaction type field is missing")
    private String type;

    @Min(value=1,message="parentId cannot be negative")
    @JsonProperty("parent_id")
    private Long parentId;

}
