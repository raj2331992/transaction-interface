package response;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class GetTransactionSumByIdResponse implements Response {
    private Double amount;

}
