package response;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class GetTransactionListByTypeResponse implements Response{
    private List<Long> transactionIds;

}
