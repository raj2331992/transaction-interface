package response;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class TransactionCreationResponse implements Response{
    private String status;
}
