package response;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class GetTransactionByIdResponse implements Response {
    private Double amount;
    private String type;
    private Long parentId;

}
